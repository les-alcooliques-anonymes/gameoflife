package oldReleases;

import javafx.util.Pair;

import java.util.ArrayList;

public class Grid{
    private int width, height;
    private Cell [][] grid;
    private   ArrayList<Pair<Integer, Integer>> coordAround;


    public Grid(int xSize, int ySize){
        this.coordAround = new ArrayList<>();
        this.coordAround.add(new Pair<>(0, 1));
        this.coordAround.add(new Pair<>(0, -1));
        this.coordAround.add(new Pair<>(-1, 0));
        this.coordAround.add(new Pair<>(1, 0));
        this.coordAround.add(new Pair<>(1, 1));
        this.coordAround.add(new Pair<>(1, -1));
        this.coordAround.add(new Pair<>(-1, 1));
        this.coordAround.add(new Pair<>(-1, -1));


        this.width=xSize;
        this.height=ySize;



        this.grid=new Cell[xSize][ySize];
        for(int i=0; i<xSize; ++i){
            for(int j=0; j<ySize; ++j){
                this.grid[i][j] = new Cell(i,j);
            }
        }
    }


    public  Cell[][] getGrid(){
        return this.grid;
    }
    public int getWidth(){
        return this.width;
    }
    public int getHeight(){
        return this.height;
    }

    private boolean indexValid(int x, int y){
        return (0 <= x &&  x < this.width && 0 <= y &&  y < this.height);
    }

    public void reloadGrid(){
        for(int i = 0; i < this.width; i++){
            for(int j = 0; j < this.height; j++){
                Cell c = this.getCell(i,j);
                if (c.isGonnaLive()) c.setAlive();
                else c.setDead();
            }
        }

    }
    public void gonnaLive(){
        for(int i = 0; i < this.width; i++){
            for(int j = 0; j < this.height; j++){
                Cell c = this.getCell(i,j);
                this.oneGonnaLive(c);
            }
        }
    }

    private void oneGonnaLive(Cell c){

        int x = c.getX();
        int y = c.getY();
        int neighboringCellAlive = 0;

        for (Pair<Integer, Integer> p : this.coordAround){
            if (this.indexValid(x+p.getKey(), y+p.getValue())){
                if (this.getCell(x+p.getKey(), y+p.getValue()).isAlive()){
                    neighboringCellAlive++;
                }

            }
        }


        if( c.isAlive() && (neighboringCellAlive == 2 || neighboringCellAlive == 3)){
            c.setGonnaLive();
        }else if (!c.isAlive() && neighboringCellAlive ==3){
            c.setGonnaLive();
        }else{
            c.setGonnaDie();
        }
    }

    public Cell getCell(int x, int y){

        return this.grid[x][y];
    }
}