package oldReleases;

import javafx.util.Pair;

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;



public class CellHashSet {
    private Set<Cell> cellHashMap;
    private ArrayList<Pair<Integer, Integer>> coordAround;


    public CellHashSet(){
        this.coordAround = new ArrayList<>();
        this.coordAround.add(new Pair<>(0, 1));
        this.coordAround.add(new Pair<>(0, -1));
        this.coordAround.add(new Pair<>(-1, 0));
        this.coordAround.add(new Pair<>(1, 0));
        this.coordAround.add(new Pair<>(1, 1));
        this.coordAround.add(new Pair<>(1, -1));
        this.coordAround.add(new Pair<>(-1, 1));
        this.coordAround.add(new Pair<>(-1, -1));

        this.cellHashMap = new HashSet<>();

    }
    public boolean addCell(Cell c){

        this.cellHashMap.add(c);
        return true;
    }


    private boolean isNotEmpty(){
        return this.cellHashMap.size() != 0;
    }
    private boolean indexValid(int x, int y){
        return true; //(0 <= x &&  x < this.width && 0 <= y &&  y < this.height);
    }

    public void reloadGrid(){
        /*
        Iterator<Cell> i = this.cellHashMap.iterator();
        while (i.hasNext()) {
            Cell c = i.next(); // must be called before you can call i.remove()
            if(!c.isGonnaLive()){
                this.cellHashMap.remove(c);
            }else{
                c.setAlive();
            }
            i.remove();
        }*/





        if (this.isNotEmpty()){
            Set<Cell> toRemove = new HashSet<>();
            //System.out.println(this.cellHashMap);
            for(Cell c : this.cellHashMap){
                if(!c.isGonnaLive()){
                    toRemove.add(c);
                }else{
                    c.setAlive();
                }
            }
            this.cellHashMap.removeAll(toRemove);
        }


    }
    public void gonnaLive(){
        if (this.isNotEmpty()) {
            for (Cell c : new HashSet<>( this.cellHashMap)) {
                this.oneGonnaSurvive(c);
            }
        }
    }

    private void oneGonnaBorn(Cell c){

        int x = c.getX();
        int y = c.getY();
        int neighboringCellAlive = 0;

        for (Pair<Integer, Integer> p : this.coordAround){
            if (this.indexValid(x+p.getKey(), y+p.getValue())){
                if (this.getCell(x+p.getKey(), y+p.getValue()).isAlive()){
                    neighboringCellAlive++;
                }

            }
        }
        if (neighboringCellAlive ==3){
           c.setGonnaLive();
           this.addCell(c);
        }
    }

    public Set<Cell> getCellHashMap() {
        return cellHashMap;
    }

    private void oneGonnaSurvive(Cell c){

        int x = c.getX();
        int y = c.getY();
        int neighboringCellAlive = 0;

        for (Pair<Integer, Integer> p : this.coordAround){
            if (this.indexValid(x+p.getKey(), y+p.getValue())){
                Cell nearCell =  this.getCell(x+p.getKey(), y+p.getValue());
                if (nearCell.isAlive()){
                    neighboringCellAlive++;
                }else{
                    this.oneGonnaBorn(nearCell);
                }

            }
        }


        if( (neighboringCellAlive == 2 || neighboringCellAlive == 3)){
            c.setGonnaLive();
        }else{
            c.setGonnaDie();
        }
    }

    public Cell getCell(int x, int y){
        if (this.isNotEmpty()) {
            for (Cell otherC : this.cellHashMap) {
                if (otherC.getX() == x && otherC.getY() == y) return otherC;
            }
        }
        Cell c =new Cell(x,y);
        c.setDead();
        return  c;
    }
}
