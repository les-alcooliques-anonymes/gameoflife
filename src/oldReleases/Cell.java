package oldReleases;

import java.util.Objects;

public class Cell {
    private boolean isAlive, gonnaLive = false;
    private Integer x,y;
    public Cell(Integer x, Integer y){
        this.x = x;
                this.y = y;
    }
    public boolean isAlive(){
        return this.isAlive;
    }

    public void setAlive(){
        this.isAlive = true;
    }
    public void setDead(){
        this.isAlive = false;
    }

    public void setGonnaDie() {
        this.gonnaLive = false;
    }
    public void setGonnaLive() {
        this.gonnaLive = true;
    }

    public boolean isGonnaLive() {
        return this.gonnaLive;
    }

    public Integer getX() {
        return this.x;
    }

    public Integer getY() {
        return this.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(x, cell.x) &&
                Objects.equals(y, cell.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
