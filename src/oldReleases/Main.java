package oldReleases;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application {

    /* Settings */
    protected int cellSizePx = 30;
    protected int marge = 200;


    /* Game variables */
    protected Grid grid;
    protected Integer xGridSize;
    protected Integer yGridSize;
    protected boolean isRunning = false;
    protected Integer numberOfPeriods = 0;
    protected Image dead,alive;
    protected long lastNanoTime;


    public static void main(String[] args) {
        launch(args);
    }

    private void drawAllCell(GraphicsContext gc){
        for(int i = 0; i < this.grid.getWidth(); i++){
            for(int j = 0; j < this.grid.getHeight(); j++){
                this.drawOneCell(gc, this.grid.getGrid()[i][j]);
            }
        }

    }
    private void drawGui(GraphicsContext gc){
        gc.setFill(Color.WHITE);
        gc.fillRect(0,0, this.marge,512);
        gc.setFill(Color.BLACK);


        Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, this.marge/5 );
        gc.setFont( theFont );

        gc.fillText("Cycles :", 10,  10+this.marge/10 );
        gc.fillText( this.numberOfPeriods.toString(), 10,  10+ +this.marge/3 );
        if (!this.isRunning){
            gc.setFill(Color.RED);
            theFont = Font.font( "Times New Roman", FontWeight.BOLD, this.marge );
            gc.setFont( theFont );
            gc.fillText("⏸", -10,  10+this.marge );
            gc.setFill(Color.BLACK);
        }
        //gc.strokeText( this.nbBomb.toString(), 10,  10+ cellSizePx*2 );
    }

    /* Actualise / place un cellule dans le cavena */
    private void drawOneCell(GraphicsContext gc, Cell c){
        if(c.isAlive()){
            gc.drawImage( this.alive, c.getX()*this.cellSizePx+this.marge, c.getY()*this.cellSizePx);
        }else{
            gc.drawImage( this.dead, c.getX()*this.cellSizePx+this.marge, c.getY()*this.cellSizePx);

        }


    }


    /* Fonction qui est appelé à chaque click de souris */
    private void clickEventInGame(int x, int y, String click, GraphicsContext gc){

            if (click == "PRIMARY"){
                Cell c = this.grid.getCell(x,y);
                if(c.isAlive()) c.setDead();
                else c.setAlive();
                this.drawOneCell(gc, c);
            }
            else if (click == "SECONDARY"){
                this.isRunning = ! this.isRunning;
                drawGui(gc);
            }


    }


    private void importImage(){

        this.dead = new Image( "img/dead.png",this.cellSizePx,this.cellSizePx,false,false);
        this.alive   = new Image( "img/alive.png",this.cellSizePx,this.cellSizePx,false,false );
    }


    @Override
    public void start(Stage primaryStage) {


        primaryStage.setTitle( "GameOfLife" );

        Group root = new Group();
        Scene theScene = new Scene( root );

        primaryStage.setMaximized(true);



       Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();


       this.xGridSize = ((int) (primaryScreenBounds.getWidth()-this.marge)) / this.cellSizePx;
        this.yGridSize = (int) primaryScreenBounds.getHeight() / this.cellSizePx;

        this.grid=new Grid(this.xGridSize,this.yGridSize);



        primaryStage.setScene( theScene );

        Canvas canvas = new Canvas( this.grid.getWidth()*this.cellSizePx+this.marge, this.grid.getHeight()*this.cellSizePx );
        root.getChildren().add( canvas );

        GraphicsContext gc = canvas.getGraphicsContext2D();

        theScene.setOnMouseClicked(
                new EventHandler<MouseEvent>()
                {
                    public void handle(MouseEvent e)
                    {
                        //System.out.println( e.getX()+", "+ e.getY() + "click  : " + e.getButton());
                        clickEventInGame((int) ( (e.getX()-marge) / cellSizePx ),  (int) (e.getY() / cellSizePx ),e.getButton().toString(), gc );
                    }
                });



        this.importImage();
        this.drawAllCell(gc);
        this.drawGui(gc);
         final long startNanoTime = System.nanoTime();
         this.lastNanoTime= System.nanoTime();
        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                /* Boucle du jeu s'éxecute 60 fois par seconde environ*/
                double t = (currentNanoTime - startNanoTime) / 1000000000000.0;


                // 200000000.0
                if (isRunning && currentNanoTime-lastNanoTime > 20000000.0){
                    lastNanoTime = System.nanoTime();
                    numberOfPeriods++;
                    grid.gonnaLive();
                    grid.reloadGrid();
                    drawAllCell(gc);
                    drawGui(gc);
                }

            }
        }.start();

        primaryStage.show();
    }
}
