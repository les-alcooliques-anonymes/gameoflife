import javafx.util.Pair;

import java.util.ArrayList;

public class GridBool {
    private int width, height;
    private boolean [][] grid;
    private   ArrayList<Pair<Integer, Integer>> coordAround;


    public GridBool(int xSize, int ySize){
        this.coordAround = new ArrayList<>();
        this.coordAround.add(new Pair<>(0, 1));
        this.coordAround.add(new Pair<>(0, -1));
        this.coordAround.add(new Pair<>(-1, 0));
        this.coordAround.add(new Pair<>(1, 0));
        this.coordAround.add(new Pair<>(1, 1));
        this.coordAround.add(new Pair<>(1, -1));
        this.coordAround.add(new Pair<>(-1, 1));
        this.coordAround.add(new Pair<>(-1, -1));


        this.width=xSize;
        this.height=ySize;



        this.grid=new boolean[xSize][ySize];
        for(int i=0; i<xSize; ++i){
            for(int j=0; j<ySize; ++j){
                this.grid[i][j] = false;
            }
        }
    }


    public  boolean[][] getGrid(){
        return this.grid;
    }
    public int getWidth(){
        return this.width;
    }
    public int getHeight(){
        return this.height;
    }

    private boolean indexValid(int x, int y){
        return (0 <= x &&  x < this.width && 0 <= y &&  y < this.height);
    }


    public void nextGen(){


        boolean[][] newGrid = new boolean[this.width][this.height];


        for(int i = 0; i < this.width; i++){
            for(int j = 0; j < this.height; j++){
                newGrid[i][j] = oneGonnaLive(i,j, this.grid[i][j]);
            }
        }
        this.grid = newGrid;

    }

    private boolean oneGonnaLive(int x, int y, boolean t){
        int neighboringCellAlive = 0;

        for (Pair<Integer, Integer> p : this.coordAround){
            if (this.indexValid(x+p.getKey(), y+p.getValue())){
                if (this.getCell(x+p.getKey(), y+p.getValue())){
                    neighboringCellAlive++;
                }

            }
        }
        if( t && (neighboringCellAlive == 2 || neighboringCellAlive == 3)){
            return true;
        }else if (!t && neighboringCellAlive ==3){
            return true;
        }else{
            return false;
        }
    }

    public boolean getCell(int x, int y){

        return this.grid[x][y];
    }
}